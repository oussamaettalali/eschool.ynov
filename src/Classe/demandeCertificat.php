<?php

namespace App\Classe;

use App\Entity\Certificat;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class demandeCertificat
{
    private $session;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, SessionInterface $session)
    {
        $this->session= $session;
        $this->entityManager= $entityManager;
    }

    public function add($id)
    {
        $this->session->set('demande', [
            'id' => $id
        ]);
    }

    public function get()
    {
        return $this->session->get('demande');
    }

    public function getAll()
    {
        foreach ($this->get() as $id) {
            $demandeList = $this->entityManager->getRepository(Certificat::class)->findById($id);
        }
        return $demandeList;
    }
}
