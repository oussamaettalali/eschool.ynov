<?php

namespace App\Entity;

use App\Repository\DemandeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemandeRepository::class)
 */
class Demande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="demandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $etudiant;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=DetailsDemande::class, mappedBy="demande")
     */
    private $detailsDemandes;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Commentaire;

    public function __construct()
    {
        $this->detailsDemandes = new ArrayCollection();
    }

    public function getCertificatName() {

        $certificatName = null;

        foreach ($this->getDetailsDemandes()->getValues() as $demandes) {
            $certificatName = $demandes->getCertificat();
        }

        return $certificatName;
    }

    public function getStartDate() {

        $Name = null;

        foreach ($this->getDetailsDemandes()->getValues() as $demandes) {
            $Name = $demandes->getStartDate();
        }

        return $Name;
    }

    public function getEndDate() {

        $Name = null;

        foreach ($this->getDetailsDemandes()->getValues() as $demandes) {
            $Name = $demandes->getEndDate();
        }

        return $Name;
    }

    public function getTheme() {

        $Name = null;

        foreach ($this->getDetailsDemandes()->getValues() as $demandes) {
            $Name = $demandes->getTheme();
        }

        return $Name;
    }

    public function getEntreprise() {

        $Name = null;

        foreach ($this->getDetailsDemandes()->getValues() as $demandes) {
            $Name = $demandes->getEntreprise();
        }

        return $Name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEtudiant(): ?User
    {
        return $this->etudiant;
    }

    public function setEtudiant(?User $etudiant): self
    {
        $this->etudiant = $etudiant;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|DetailsDemande[]
     */
    public function getDetailsDemandes(): Collection
    {
        return $this->detailsDemandes;
    }

    public function addDetailsDemande(DetailsDemande $detailsDemande): self
    {
        if (!$this->detailsDemandes->contains($detailsDemande)) {
            $this->detailsDemandes[] = $detailsDemande;
            $detailsDemande->setDemande($this);
        }

        return $this;
    }

    public function removeDetailsDemande(DetailsDemande $detailsDemande): self
    {
        if ($this->detailsDemandes->removeElement($detailsDemande)) {
            // set the owning side to null (unless already changed)
            if ($detailsDemande->getDemande() === $this) {
                $detailsDemande->setDemande(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->Commentaire;
    }

    public function setCommentaire(?string $Commentaire): self
    {
        $this->Commentaire = $Commentaire;

        return $this;
    }
}
