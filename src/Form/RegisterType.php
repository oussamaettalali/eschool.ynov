<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom',
                'attr' => [
                    'placeholder' => ""
                ]
            ])
            ->add('prenom', TextType::class, [
                'label' => 'Prénom',
                'attr' => [
                    'placeholder' => ""
                ]
            ])
            ->add('date_naissance', BirthdayType::class, [
                'label' => 'Date de naissance',
                'placeholder' => [
                    'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                ]
            ])
            ->add('ville_naissance')
            ->add('pays_naissance', CountryType::class, [
                'label' => 'Pays de naissance',
                'placeholder' => 'Selectionner un pays',
            ])
            ->add('sexe', ChoiceType::class, [
                'label' => 'Sexe',
                'choices' => [
                    'Homme' => 'Homme',
                    'Femme' => 'Femme',
                ],
            ])
            ->add('cne')
            ->add('cin')
            ->add('adresse')
            ->add('code_bac')
            ->add('serie_bac')
            ->add('annee_1ere_inscription_universite', TextType::class, [
                'label' => 'Année 1ère inscription université',
            ])
            ->add('annee_1ere_inscription_enseignement_superieur', TextType::class, [
                'label' => 'Année 1ère inscription enseignement superieur',
            ])
            ->add('annee_1ere_inscription_universite_marocaine', TextType::class, [
                'label' => 'Année 1ère inscription université marocaine',
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse E-mail'
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passes doivent être identiques',
                'required' => true,
                'first_options' => [ 'label' => 'Mot de passe'],
                'second_options' => [ 'label' => 'Confirmez votre mot de passe']
            ])
            ->add('submit', SubmitType::class, [
                'label' => "S'INSCRIRE"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
