<?php

namespace App\Form;

use App\Entity\Certificat;
use App\Entity\DetailsDemande;
use App\Repository\CertificatRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('certificat',  EntityType::class, array(
                'label' => 'Certificat',
                'class' => 'App\Entity\Certificat',
                'query_builder' => function (CertificatRepository $cr) {
                    return $cr->createQueryBuilder('c')
                        ->andWhere('c.categorie = :categorie')
                        ->setParameter('categorie', 2);
                },
                'choice_label' => 'nom'
            ))
            ->add('StartDate', DateType::class, [
                'label' => 'Date de début',
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
            ])
            ->add('EndDate', DateType::class, [
                'label' => 'Date de fin',
                'widget' => 'single_text',
                // this is actually the default format for single_text
                'format' => 'yyyy-MM-dd',
            ])
            ->add('theme', TextType::class, [
                'label' => 'Theme'
            ])
            ->add('entreprise', TextType::class, [
                'label' => 'Entreprise'
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'DEMANDER'



            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
