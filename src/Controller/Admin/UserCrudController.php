<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index', 'Étudiants')
            ->setPageTitle('detail', 'Fiche étudiant')
            ->setPageTitle('edit', 'Modifier infos étudiant')
            ->setPageTitle('new', 'Ajouter infos étudiant')
            ->setEntityLabelInSingular('Étudiant')
            ->setEntityLabelInPlural('Étudiants');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add('index', 'detail');
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('getNomComplet', 'Nom & Prénom')->hideOnForm(),
            TextField::new('nom','Nom')->onlyOnForms(),
            TextField::new('prenom','Prénom')->onlyOnForms(),
            DateTimeField::new('date_naissance', 'Date de naissance')
                ->setFormType(BirthdayType::class)
                ->setFormat('dd-MM-yyyy'),
            TextField::new('sexe')
                ->setFormType(ChoiceType::class)
                ->setFormTypeOptions([
                    'choices' => [
                        'Masculin' => "Masculin",
                        'Féminin' => "Féminin",
                    ],
                ]),
            TextField::new('ville_naissance', 'Ville de naissance')->hideOnIndex(),
            CountryField::new('pays_naissance', 'Pays de naissance')->hideOnIndex()
                ->setFormType(CountryType::class)
                ->setFormTypeOptions(['placeholder' => 'Selectionner un pays']),
            TextField::new('cne', 'CNE')->hideOnIndex(),
            TextField::new('cin', 'CIN')->hideOnIndex(),
            TextField::new('adresse', 'Adresse postal')->hideOnIndex(),
            TextField::new('code_bac', 'Code Bac')->hideOnIndex(),
            TextField::new('serie_bac', 'Série Bac')->hideOnIndex(),
            IntegerField::new('annee_1ere_inscription_universite', 'Année 1ère inscription université')->hideOnIndex(),
            IntegerField::new('annee_1ere_inscription_enseignement_superieur', 'Année 1ère inscription enseignement superieur')->hideOnIndex(),
            IntegerField::new('annee_1ere_inscription_universite_marocaine', 'Année 1ère inscription marocaine')->hideOnIndex(),
            EmailField::new('email', 'Email')->hideOnIndex(),
            TextField::new('password', 'Password')->onlyWhenCreating(),
        ];
    }
}
