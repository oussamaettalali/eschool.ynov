<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Certificat;
use App\Entity\Demande;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(DemandeCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('eSchool - Ynov Maroc');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Demandes', 'fas fa-list-ul', Demande::class);
        yield MenuItem::linkToCrud('Étudiants', 'fa fa-user', User::class);
        yield MenuItem::linkToCrud('Catégories', 'fas fa-layer-group', Category::class);
        yield MenuItem::linkToCrud('Certificats', 'fas fa-clipboard', Certificat::class);
    }
}
