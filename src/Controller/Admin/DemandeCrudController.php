<?php

namespace App\Controller\Admin;

use App\Classe\Mail;
use App\Entity\Demande;
use App\Classe\demandeCertificat;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;

class DemandeCrudController extends AbstractCrudController
{
    private $entityManager;
    private $crudUrlGenerator;

    public function __construct(EntityManagerInterface $entityManager, CrudUrlGenerator $crudUrlGenerator)
    {
        $this->entityManager = $entityManager;
        $this->crudUrlGenerator = $crudUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Demande::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureActions(Actions $actions): Actions
    {
        $preparation = Action::new('preparation', 'En cours de préparation')->linkToCrudAction('preparation');
        $pret = Action::new('pret', 'Prêt(e) à être retiré')->linkToCrudAction('pret');
        $refus = Action::new('refus', 'Refusé(e)')->linkToCrudAction('refus');
        $pdf = Action::new('pdf', 'PDF', 'fas fa-file-pdf')->linkToCrudAction('pdf');

        return $actions
            ->add('detail', $refus)
            ->add('detail', $pret)
            ->add('detail', $preparation)
            ->add('index', 'detail')
            ->add('detail', $pdf)
            ->disable(Action::NEW, Action::DELETE);
    }


    public function preparation(AdminContext $context)
    {
        $Demande = $context->getEntity()->getInstance();
        $Demande->setStatus(1);
        $this->entityManager->flush();

        $mail = new Mail();
        $content = "Bonjour " .$Demande->getEtudiant()->getPrenom()."<br/>Votre certificat est en cours de préparation<br><br/> Consulter l'état de votre demande sur votre espace etudiant en cliquant lien ci-dessous";
        $mail->send($Demande->getEtudiant()->getEmail(), $Demande->getEtudiant()->getPrenom(), "Votre document est en cours de préparation", $content);


        $this->addFlash('notice',"<span><strong>La demande N° ".$Demande->getId(). " est en cours de préparation</strong></span>");

        $url = $this->crudUrlGenerator->build()
            ->setController(DemandeCrudController::class)
            ->setAction('detail')
            ->generateUrl();

        return $this->redirect($url);
    }

    public function pret(AdminContext $context)
    {
        $Demande = $context->getEntity()->getInstance();
        $Demande->setStatus(2);
        $this->entityManager->flush();


        $mail = new Mail();
        $content = "Bonjour " .$Demande->getEtudiant()->getPrenom()."<br/>Votre document est prêt à être retiré<br><br/> Passez à l'administration pour le récuperer";
        $mail->send($Demande->getEtudiant()->getEmail(), $Demande->getEtudiant()->getPrenom(), "Votre document est prêt à être retiré", $content);


        $this->addFlash('notice',"<span><strong>Le certificat N° ".$Demande->getId(). " est prêt à être retiré</strong></span>");

        $url = $this->crudUrlGenerator->build()
            ->setController(DemandeCrudController::class)
            ->setAction('detail')
            ->generateUrl();

        return $this->redirect($url);
    }

    public function refus(AdminContext $context)
    {
        $Demande = $context->getEntity()->getInstance();
        $Demande->setStatus(3);
        $this->entityManager->flush();

        $mail = new Mail();
        $content = "Bonjour " .$Demande->getEtudiant()->getPrenom()."<br/>Votre demande a été refusé<br><br/>Pour plus d'informations, contacter l'administration";
        $mail->send($Demande->getEtudiant()->getEmail(), $Demande->getEtudiant()->getPrenom(), "Votre demande a été refusée", $content);


        $this->addFlash('notice',"<span><strong>La demande N° ".$Demande->getId(). " a été refusée</strong></span>");

        $url = $this->crudUrlGenerator->build()
            ->setController(DemandeCrudController::class)
            ->setAction('detail')
            ->generateUrl();

        return $this->redirect($url);
    }

    public function pdf(AdminContext $context)
    {
        $Demande = $context->getEntity()->getInstance();

        //Datefolder
        $date = new \DateTime();
        $dateFormat = $date->format('d-m-Y');

        // Testing variables
        $etudiant = $Demande->getEtudiant()->getNomComplet();
        $naissance = $Demande->getEtudiant()->getDateNaissance();
        $certDir = "certs";
        $CertDemande = null;
        $CertName = $Demande->getCertificatName();
        $CertDate = $Demande->getCreatedAt();


        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        $html = $this->renderView('pdfview/test.html.twig', [
            'etudiant' => $etudiant,
            'demandes' => $Demande,
            'certname' => $CertName,
            'certdate' => $CertDate,
            'currentdate' => $date,
            'naissance' => $naissance
        ]);
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

                // Store PDF Binary Data
                $output = $dompdf->output();

                // Create folder if not exist
                if (!file_exists("{$certDir}")) {
                    mkdir("{$certDir}", 0777, true);
                }

                if (!file_exists("{$certDir}/{$dateFormat}")) {
                    mkdir("{$certDir}/{$dateFormat}", 0777, true);
                }

                // In this case, we want to write the file in the public directory
                $publicDirectory = $this->getParameter('kernel.project_dir'). "/public/{$certDir}/{$dateFormat}";
                // e.g /var/www/project/public/mypdf.pdf
                $pdfFilepath =  $publicDirectory . "/{$etudiant}.pdf";

                // Write file to the desired path
                file_put_contents($pdfFilepath, $output);
        ob_end_clean();
        $dompdf->stream("/{$etudiant}.pdf", [
            "Attachment" => false
        ]);

        // Send some text response
        return new Response("The PDF file has been succesfully generated !");

        /* return $this->render('pdfview/pdf.html.twig', [
             'controller_name' => 'PdfviewController',
         ]);*/
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('etudiant.getNomComplet', 'Étudiant')
                ->setFormType(TextType::class)
                ->setFormTypeOptions(['disabled' => true]),
            TextField::new('getCertificatName', 'Demande')
                ->setFormType(TextType::class)
                ->setFormTypeOptions(['disabled' => true]),
            DateTimeField::new('createdAt', 'Faite le')
                ->setFormType(DateTimeType::class)
                ->setFormat('dd-MM-yyyy HH:mm')
                ->setFormTypeOptions(['disabled' => true]),
            ChoiceField::new('status')->setChoices([
                'En attente de traitement' => 0,
                'En cours de préparation' => 1,
                'Prêt(e) à être retiré(e)' => 2,
                'Refusé(e)' => 3
            ]),
            TextField::new('commentaire'),
            DateTimeField::new('getStartDate', 'Date de début')->hideOnIndex()
                ->setFormType(DateTimeType::class)
                ->setFormat('dd-MM-yyyy'),
            DateTimeField::new('getEndDate', 'Date de fin')->hideOnIndex()
                ->setFormType(DateTimeType::class)
                ->setFormat('dd-MM-yyyy'),
            TextField::new('getTheme', 'Theme')->hideOnIndex()
                ->setFormType(TextType::class),
            TextField::new('getEntreprise', 'Entreprise')->hideOnIndex()
                ->setFormType(TextType::class),
        ];
    }

}