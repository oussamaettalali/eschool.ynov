<?php

namespace App\Controller;

use App\Classe\demandeCertificat;
use App\Entity\Demande;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountDemandeController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/app/compte/mes-demandes", name="account_demande")
     */
    public function index(): Response
    {
        $etudiant = $this->getUser();
        $demandes = $this->entityManager->getRepository(Demande::class)->findOrderedAll($etudiant);

        return $this->render('account/demande.html.twig', [
            'demandes' => $demandes,
        ]);
    }
}
