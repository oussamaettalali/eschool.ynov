<?php

namespace App\Controller;


use App\Classe\demandeCertificat;
use App\Classe\Mail;
use App\Entity\Demande;
use App\Entity\DetailsDemande;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DemandeController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/app/demande/", name="demande")
     */
    public function index(demandeCertificat $demandeCert): Response
    {
        $notification = null;
        $date = new \DateTime();

        $Demande = new Demande();
        $Demande->setEtudiant($this->getUser());
        $Demande->setCreatedAt($date);
        $Demande->setStatus(0);

        $this->entityManager->persist($Demande);

        foreach($demandeCert->getAll() as $DemandeDetails) {
            $DemandeFinal = new DetailsDemande();
            $DemandeFinal->setDemande($Demande);
            $DemandeFinal->setCertificat($DemandeDetails->getNom());
            $this->entityManager->persist($DemandeFinal);
        }

        $notification = "Votre demande a été envoyée avec succès !";

        $this->entityManager->flush();

        //Envoyer un mail à l'étudiant pour lui confirmer la réception de sa demande
        $mail = new Mail();
        $content = "Bonjour " .$Demande->getEtudiant()->getNom()."<br/>Votre demande: ".$DemandeFinal->getCertificat()." a été enregistré avec succès<br><br/> Consulter l'état de votre demande en cliquant lien ci-dessous";
        $mail->send($Demande->getEtudiant()->getEmail(), $Demande->getEtudiant()->getNom(), "Nous vous confirmons la réception de votre demande", $content);

        //Envoyer un mail à l'admin pour lui annoncer la réception d'une nouvelle demande
        $mailAdmin = new Mail();
        $contentForAdmin = "Une nouvelle demande de " .$DemandeFinal->getCertificat(). " a été envoyé par " .$Demande->getEtudiant()->getNomComplet();
        $mailAdmin->send('oussamaettalali@gmail.com', 'Oussama Ettalali', 'Nouvelle demande de '.$DemandeFinal->getCertificat(), $contentForAdmin);



        return $this->render('demande/index.html.twig', [
            'demandeDetails' => $demandeCert->getAll(),
            'demande' => $Demande,
            'notification' => $notification
        ]);
    }

    /**
     * @Route("/app/demande/add/{id}", name="add_demande")
     */
    public function add(demandeCertificat $demandeCert, $id): Response
    {
        $demandeCert->add($id);

        return $this->redirectToRoute('demande');
    }
}
