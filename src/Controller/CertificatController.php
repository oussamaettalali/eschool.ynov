<?php

namespace App\Controller;

use App\Classe\Mail;
use App\Entity\Certificat;
use App\Entity\Demande;
use App\Entity\DetailsDemande;
use App\Entity\User;
use App\Form\RegisterType;
use App\Form\StageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CertificatController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/app/certificats-scolarite", name="certificats-scolarite")
     */
    public function scolarite(): Response
    {
        $categorie = 1;
        $certificats = $this->entityManager->getRepository(Certificat::class)->findScolaireAll($categorie);

        return $this->render('certificat/scolarite.html.twig', [
            'certificats' => $certificats
        ]);
    }

    /**
     * @Route("/app/certificats-stage", name="certificats-stage")
     */
    public function stage(Request $request)
    {
        $notification =null;

        $certificatDetails = null;
        $user = $this->getUser();
        $date = new \DateTime();

        $demandeStage = new Demande();
        $demandeStage->setEtudiant($user);
        $demandeStage->setCreatedAt($date);
        $demandeStage->setStatus(0);
        $this->entityManager->persist($demandeStage);


        $form = $this->createForm(StageType::class);

        $form -> handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //$DemandeDetails = $form->getData();
            $StartDate = $form->get('StartDate')->getData();
            $EndDate = $form->get('EndDate')->getData();
            $Theme = $form->get('theme')->getData();
            $Entreprise = $form->get('entreprise')->getData();
            $certificatDetails = $form->get('certificat')->getData();
            //$CategorieDetails = $certificatDetails->getCategorie()->getNom();

            $DemandeFinale = new DetailsDemande();
            $DemandeFinale->setDemande($demandeStage);
            $DemandeFinale->setCertificat($certificatDetails->getNom());
            $DemandeFinale->setStartDate($StartDate);
            $DemandeFinale->setEndDate($EndDate);
            $DemandeFinale->setTheme($Theme);
            $DemandeFinale->setEntreprise($Entreprise);

            $notification = "Votre demande a été envoyée avec succès !";

            $this->entityManager->persist($DemandeFinale);
            $this->entityManager->flush();


            //Envoyer un mail à l'étudiant pour lui confirmer la réception de sa demande
            $mail = new Mail();
            $content = "Bonjour " .$demandeStage->getEtudiant()->getNom()."<br/>Votre demande: ".$DemandeFinale->getCertificat()." a été enregistré avec succès<br><br/> Consulter l'état de votre demande en cliquant lien ci-dessous";
            $mail->send($demandeStage->getEtudiant()->getEmail(), $demandeStage->getEtudiant()->getNom(), "Nous vous confirmons la réception de votre demande", $content);

            //Envoyer un mail à l'admin pour lui annoncer la réception d'une nouvelle demande
            $mailAdmin = new Mail();
            $contentForAdmin = "Une nouvelle demande : " .$DemandeFinale->getCertificat(). " a été envoyé par " .$demandeStage->getEtudiant()->getNomComplet();
            $mailAdmin->send('oussamaettalali@gmail.com', 'Oussama Ettalali', 'Nouvelle demande : '.$DemandeFinale->getCertificat(), $contentForAdmin);

        }

        return $this->render('certificat/stage.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification,
            'certificat' => $certificatDetails,
            'demande' => $demandeStage,
        ]);
    }
}
